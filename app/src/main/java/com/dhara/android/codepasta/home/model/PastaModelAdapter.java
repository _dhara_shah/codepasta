package com.dhara.android.codepasta.home.model;

import android.support.annotation.ColorRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public interface PastaModelAdapter {
    int getCount();

    @NonNull String getPastaName(int position);

    @NonNull String getPastaDescription(int position);

    @NonNull String getPastaOwnerName(int position);

    @ColorRes
    int getBackgroundColor(int position);

    String getAvatar(int position);

    String getId(int position);

    String getName();

    String getEmail();

    @Visibility
    int getFabVisibility();

    @Visibility
    int getForksVisibility(int position);

    @IntDef({VISIBLE, INVISIBLE, GONE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Visibility {}
}
