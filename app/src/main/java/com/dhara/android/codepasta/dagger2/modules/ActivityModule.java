package com.dhara.android.codepasta.dagger2.modules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.dhara.android.codepasta.dagger2.injection.PerActivity;
import com.dhara.android.codepasta.dagger2.scope.ActivityScope;
import com.dhara.android.codepasta.home.presenter.HomePresenter;
import com.dhara.android.codepasta.home.presenter.HomePresenterImpl;
import com.dhara.android.codepasta.home.router.HomeRouter;
import com.dhara.android.codepasta.home.router.HomeRouterImpl;
import com.dhara.android.codepasta.home.view.HomeView;
import com.dhara.android.codepasta.home.view.HomeViewImpl;
import com.dhara.android.codepasta.login.presenter.LoginPresenter;
import com.dhara.android.codepasta.login.presenter.LoginPresenterImpl;
import com.dhara.android.codepasta.login.router.LoginRouter;
import com.dhara.android.codepasta.login.router.LoginRouterImpl;
import com.dhara.android.codepasta.login.view.LoginView;
import com.dhara.android.codepasta.login.view.LoginViewImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    Context providesContext() {
        return activity;
    }

    @Provides
    @PerActivity
    public HomePresenter provideHomePresenter(){
        return new HomePresenterImpl();
    }

    @Provides
    @PerActivity
    public LoginPresenter provideLoginPresenter() {
        return new LoginPresenterImpl();
    }

    @Provides
    @PerActivity public HomeView provideHomeView() {
        return new HomeViewImpl(activity);
    }

    @Provides
    @PerActivity public LoginView provideLoginView() {
        return new LoginViewImpl(activity);
    }

    @Provides
    @PerActivity public LoginRouter provideLoginRouter() {
        return new LoginRouterImpl(activity);
    }

    @Provides
    @PerActivity public HomeRouter provideHomeRouter() {
        return new HomeRouterImpl(activity);
    }
}