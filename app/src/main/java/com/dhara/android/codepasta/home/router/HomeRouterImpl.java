package com.dhara.android.codepasta.home.router;

import android.support.v7.app.AppCompatActivity;

import com.dhara.android.codepasta.login.view.LoginActivity;

import javax.inject.Inject;

public class HomeRouterImpl implements HomeRouter {
    private AppCompatActivity activity;

    @Inject
    public HomeRouterImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void close() {
        activity.finish();
    }

    @Override
    public void handleOnBackPress() {
        close();
    }

    @Override
    public void handleSignOut() {
        LoginActivity.startActivity(activity);
        close();
    }
}
