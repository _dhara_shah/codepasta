package com.dhara.android.codepasta.login.view;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.R;
import com.dhara.android.codepasta.network.entity.UserAuth;

import javax.inject.Inject;

public class LoginViewImpl implements LoginView {
    private AppCompatActivity activity;
    private Button btnLogin;
    private EditText etUserName;
    private EditText etPassword;
    private TextView txtRegister;

    @Inject
    public LoginViewImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(ViewInteractionListener listener) {
        btnLogin = activity.findViewById(R.id.btn_login);
        etUserName = activity.findViewById(R.id.et_username);
        etPassword = activity.findViewById(R.id.et_password);
        txtRegister = activity.findViewById(R.id.txt_register);

        btnLogin.setOnClickListener(v -> {
            if (isValid()) {
                listener.onLoginClicked(getCredentials());
            }
        });

        txtRegister.setOnClickListener(v -> {
            listener.onRegisterClicked();
        });
    }

    @Override
    public void showErrorMessage(final String errorMessage) {
        Snackbar.make(btnLogin, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    private UserAuth getCredentials() {
        return new UserAuth(etUserName.getText().toString(), etPassword.getText().toString());
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(etUserName.getText().toString())) {
            showErrorMessage(CodePastaApp.getInstance().getString(R.string.error_username_cannot_be_empty));
            return false;
        }

        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            showErrorMessage(CodePastaApp.getInstance().getString(R.string.error_password_cannot_be_empty));
            return false;
        }

        return true;
    }
}
