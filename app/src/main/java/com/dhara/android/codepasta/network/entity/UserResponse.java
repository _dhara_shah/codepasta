package com.dhara.android.codepasta.network.entity;

import android.text.TextUtils;

public class UserResponse {
    private String result;
    private User user;

    public String getResult() {
        return TextUtils.isEmpty(result) ? "" : result;
    }

    public User getUser() {
        return user == null ? new User() : user;
    }
}
