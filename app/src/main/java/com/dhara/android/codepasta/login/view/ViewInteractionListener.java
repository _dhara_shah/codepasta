package com.dhara.android.codepasta.login.view;

import com.dhara.android.codepasta.network.entity.UserAuth;

public interface ViewInteractionListener {
    void onLoginClicked(UserAuth userAuth);

    void onRegisterClicked();
}
