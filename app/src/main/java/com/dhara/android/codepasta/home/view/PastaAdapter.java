package com.dhara.android.codepasta.home.view;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dhara.android.codepasta.R;
import com.dhara.android.codepasta.home.model.PastaModelAdapter;

final class PastaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final PastaModelAdapter modelAdapter;

    public static PastaAdapter createUsing(@NonNull final PastaModelAdapter modelAdapter) {
        return new PastaAdapter(modelAdapter);
    }

    private PastaAdapter(@NonNull final PastaModelAdapter modelAdapter) {
        this.modelAdapter = modelAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pasta_individual, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder vh, final int position) {
        final ViewHolder holder = (ViewHolder) vh;
        holder.txtRepoName.setText(modelAdapter.getPastaName(position));
        holder.txtRepoDescription.setText(modelAdapter.getPastaDescription(position));
        holder.txtOwnerName.setText(modelAdapter.getPastaOwnerName(position));
        holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(),
                modelAdapter.getBackgroundColor(position)));
        holder.txtForks.setText(holder.itemView.getContext().getString(R.string.icon_forks_only));
        holder.txtForks.setVisibility(modelAdapter.getForksVisibility(position));

        Glide.with(vh.itemView.getContext())
                .load(modelAdapter.getAvatar(position))
                .apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.ic_account_circle_black))
                .into(holder.imgAvatar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.itemView.setTransitionName(modelAdapter.getId(position));
        }
    }

    @Override
    public int getItemCount() {
        return modelAdapter.getCount();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtRepoName;
        TextView txtRepoDescription;
        TextView txtOwnerName;
        TextView txtForks;
        ImageView imgAvatar;

        ViewHolder(final View itemView) {
            super(itemView);
            txtRepoName = itemView.findViewById(R.id.txt_repo_name);
            txtRepoDescription =   itemView.findViewById(R.id.txt_description);
            txtOwnerName =   itemView.findViewById(R.id.txt_owner_login);
            txtForks = itemView.findViewById(R.id.txt_forks);
            imgAvatar = itemView.findViewById(R.id.img_avatar);
        }
    }
}
