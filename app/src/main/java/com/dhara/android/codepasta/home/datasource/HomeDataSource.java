package com.dhara.android.codepasta.home.datasource;

import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.entity.Pasta;

import java.util.List;

public interface HomeDataSource {
    void fetchPastas(String userName, String userToken, Listener<List<Pasta>> listener);

    void fetchCommunityPastas(String userName, String userToken, Listener<List<Pasta>> listener);
}
