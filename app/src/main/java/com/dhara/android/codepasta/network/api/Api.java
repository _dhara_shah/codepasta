package com.dhara.android.codepasta.network.api;

public class Api {
    private static final String BASE_URL = "http://mock.api";
    public static final String RESULT_OK = "ok";

    private Api() { }

    public static String getHost() {
        return BASE_URL;
    }
}
