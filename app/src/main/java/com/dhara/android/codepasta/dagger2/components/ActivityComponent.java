package com.dhara.android.codepasta.dagger2.components;

import com.dhara.android.codepasta.dagger2.injection.PerActivity;
import com.dhara.android.codepasta.dagger2.modules.ActivityModule;
import com.dhara.android.codepasta.home.view.HomeActivity;
import com.dhara.android.codepasta.login.view.LoginActivity;

import dagger.Component;

@PerActivity
@Component(modules = {ActivityModule.class}, dependencies = {AppComponent.class, CodePastaComponent.class})
public interface ActivityComponent {
    void inject(LoginActivity activity);

    void inject(HomeActivity homeActivity);
}
