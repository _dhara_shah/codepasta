package com.dhara.android.codepasta.network.entity;

import com.google.gson.annotations.SerializedName;

public class PastaOwner {
    @SerializedName("login")
    private String ownerName;
    private long id;
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public long getId() {
        return id;
    }

    public String getOwnerName() {
        return ownerName;
    }
}
