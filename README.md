# README #

### What is this repository for? ###

* CodePasta : Ideally an application that should allow users to create pastas and view other pastas
* Version : 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Need the latest android studio with Java 1.8 support.
* The rest should be the same like every other application.

### Dependencies
1. Mockito and PowerMockito for testing
2. Android support libraries
3. Glide for image loading
4. Iconic Fonts
5. Dagger2 for dependency injections
6. Retrofit with RxJava

### Deployment instructions
1. Run the project just like every other project and you should be done.

### Contribution guidelines ###

* MVP with Interactors and Router has been used as the architectural design
* Junit tests have been written
* Follow android and java coding standards

### Who do I talk to? ###

* Dhara Shah
* Contact: sdhara2@hotmail.com