package com.dhara.android.codepasta.login.model;

import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.network.entity.User;
import com.dhara.android.codepasta.network.entity.UserAuth;

public interface LoginInteractor {
    void setUserAuth(UserAuth userAuth);

    void register(ResponseListener listener);

    User getUser();
}
