package com.dhara.android.codepasta.login.router;

import android.support.annotation.NonNull;

import com.dhara.android.codepasta.network.entity.User;

public interface LoginRouter {
    void close();

    void navigateToHome(@NonNull User user);

    void navigateToRegistration();
}
