package com.dhara.android.codepasta.dagger2.components;

import com.dhara.android.codepasta.dagger2.modules.CodePastaModule;
import com.dhara.android.codepasta.home.model.HomeInteractorImpl;
import com.dhara.android.codepasta.home.presenter.HomePresenterImpl;
import com.dhara.android.codepasta.login.model.LoginInteractorImpl;
import com.dhara.android.codepasta.login.presenter.LoginPresenterImpl;

import dagger.Component;

@Component(modules = {CodePastaModule.class})
public interface CodePastaComponent {
    void inject(HomePresenterImpl presenter);

    void inject(HomeInteractorImpl mainInteractor);

    void inject(LoginPresenterImpl loginPresenter);

    void inject(LoginInteractorImpl loginInteractor);
}
