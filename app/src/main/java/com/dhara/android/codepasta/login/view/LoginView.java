package com.dhara.android.codepasta.login.view;

public interface LoginView {
    void initViews(ViewInteractionListener listener);

    void showErrorMessage(String errorMessage);
}
