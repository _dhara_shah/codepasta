package com.dhara.android.codepasta.utils;

public abstract class ConstantIntentExtra {
    private static final String PACKAGE_NAME = ConstantIntentExtra.class.getPackage().getName();
    public static final String USER_EXTRA = PACKAGE_NAME + "_USER_EXTRA";
}
