package com.dhara.android.codepasta.home.view;

import android.support.annotation.NonNull;

import com.dhara.android.codepasta.home.model.PastaModelAdapter;

public interface HomeView {
    void initViews(@NonNull PastaModelAdapter modelAdapter, ViewInteractionListener listener);

    void showProgress();

    void hideProgress();

    void showError(String errorMessage);

    void updateData(@NonNull PastaModelAdapter modelAdapter);

    boolean closeDrawerLayout();
}
