package com.dhara.android.codepasta.network.api;

import com.dhara.android.codepasta.network.entity.Pasta;
import com.dhara.android.codepasta.network.entity.UserAuth;
import com.dhara.android.codepasta.network.entity.UserResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApi {
    @GET("/myrepositories")
    Observable<List<Pasta>> getOwnRepositories(@Query(value = "user_name", encoded = true) String userName,
                                               @Query(value = "user_token", encoded = true) String userToken);

    @GET("/repositories")
    Observable<List<Pasta>> getRepositories(@Query(value = "user_name", encoded = true) String userName,
                                            @Query(value = "user_token", encoded = true) String userToken);

    @POST("/register")
    Call<UserResponse> register(@Body UserAuth userAuth);

}