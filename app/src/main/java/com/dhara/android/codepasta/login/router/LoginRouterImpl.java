package com.dhara.android.codepasta.login.router;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.dhara.android.codepasta.home.view.HomeActivity;
import com.dhara.android.codepasta.network.entity.User;
import com.dhara.android.codepasta.utils.ConstantIntentExtra;

import javax.inject.Inject;

public class LoginRouterImpl implements LoginRouter {
    private AppCompatActivity activity;

    @Inject
    public LoginRouterImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void close() {
        activity.finish();
    }

    @Override
    public void navigateToHome(@NonNull final User user) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(ConstantIntentExtra.USER_EXTRA, user);
        HomeActivity.startActivity(activity, bundle);
        close();
    }

    @Override
    public void navigateToRegistration() {

    }
}
