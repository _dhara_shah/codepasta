package com.dhara.android.codepasta;

import android.app.Application;
import android.content.Context;

import com.dhara.android.codepasta.dagger2.AppComponentProvider;
import com.dhara.android.codepasta.dagger2.components.AppComponent;
import com.dhara.android.codepasta.dagger2.components.CodePastaComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerAppComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerCodePastaComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerNetComponent;
import com.dhara.android.codepasta.dagger2.components.NetComponent;
import com.dhara.android.codepasta.dagger2.modules.AppModule;
import com.dhara.android.codepasta.dagger2.modules.CodePastaModule;
import com.dhara.android.codepasta.dagger2.modules.NetModule;
import com.dhara.android.codepasta.network.api.Api;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class CodePastaApp extends Application implements AppComponentProvider {
    private static CodePastaApp INSTANCE;
    private AppComponent appComponent;
    private NetComponent netComponent;
    private CodePastaComponent codePastaComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        Realm.init(INSTANCE);
        final RealmConfiguration config = new RealmConfiguration
                .Builder()
                .inMemory()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

        getAppComponent().inject(this);
    }

    public static CodePastaApp getInstance() {
        return INSTANCE;
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    public CodePastaComponent getCodePastaComponent() {
        if (codePastaComponent == null) {
            codePastaComponent = DaggerCodePastaComponent.builder()
                    .codePastaModule(new CodePastaModule())
                    .build();
        }
        return codePastaComponent;
    }

    public NetComponent getNetComponent() {
        if (netComponent == null) {
            netComponent = DaggerNetComponent.builder()
                    .appModule(new AppModule(this))
                    .netModule(new NetModule(Api.getHost()))
                    .build();
        }
        return netComponent;
    }

    @Override
    public AppComponent getAppComponent(Context context) {
        return getAppComponent();
    }
}
