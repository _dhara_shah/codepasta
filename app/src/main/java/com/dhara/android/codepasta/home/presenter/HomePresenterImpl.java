package com.dhara.android.codepasta.home.presenter;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.home.model.HomeInteractor;
import com.dhara.android.codepasta.home.model.PastaModelAdapterImpl;
import com.dhara.android.codepasta.home.model.UserDataModel;
import com.dhara.android.codepasta.home.router.HomeRouter;
import com.dhara.android.codepasta.home.view.HomeView;
import com.dhara.android.codepasta.home.view.ViewInteractionListener;
import com.dhara.android.codepasta.network.ServiceError;

import javax.inject.Inject;

public class HomePresenterImpl implements HomePresenter, ViewInteractionListener, ResponseListener {
    private HomeView view;
    private UserDataModel model;
    private HomeRouter router;

    @Inject
    HomeInteractor interactor;

    public HomePresenterImpl() {
        CodePastaApp.getInstance().getCodePastaComponent().inject(this);
    }

    @Override
    public void handleOnCreate(final UserDataModel model) {
        this.model = model;
    }

    @Override
    public void attach(final HomeView view, final HomeRouter router) {
        this.view = view;
        this.router = router;
        this.view.initViews(PastaModelAdapterImpl.createUsing(interactor.getPastas(), model), this);
        fetchData();
    }

    @Override
    public void handleOnBackPress() {
        if (!view.closeDrawerLayout()) {
            router.handleOnBackPress();
        }
    }

    @Override
    public void onCommPastaSelected() {
        model.setOwner(false);
        fetchData();
    }

    @Override
    public void onMyPastasSelected() {
        model.setOwner(true);
        fetchData();
    }

    @Override
    public void onSignedOut() {
        router.handleSignOut();
    }

    @Override
    public void onSuccess() {
        view.hideProgress();
        view.updateData(PastaModelAdapterImpl.createUsing(interactor.getPastas(), model));
    }

    @Override
    public void onError(final ServiceError error) {
        view.hideProgress();
        view.showError(error.getErrorMessage());
    }

    private void fetchData() {
        view.showProgress();
        interactor.loadPastas(model.getUser().getName(),
                model.getUser().getUserToken(),
                model.isOwner(),
                this );
    }
}
