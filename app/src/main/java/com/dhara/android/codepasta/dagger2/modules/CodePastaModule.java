package com.dhara.android.codepasta.dagger2.modules;

import com.dhara.android.codepasta.home.datasource.HomeDataSource;
import com.dhara.android.codepasta.home.datasource.HomeDataSourceImpl;
import com.dhara.android.codepasta.home.model.HomeInteractor;
import com.dhara.android.codepasta.home.model.HomeInteractorImpl;
import com.dhara.android.codepasta.login.datasource.LoginDataSource;
import com.dhara.android.codepasta.login.datasource.LoginDataSourceImpl;
import com.dhara.android.codepasta.login.model.LoginInteractor;
import com.dhara.android.codepasta.login.model.LoginInteractorImpl;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;

@Module
public class CodePastaModule {
    @Provides
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    public HomeInteractor provideHomeInteractor() {
        return new HomeInteractorImpl();
    }

    @Provides
    public LoginInteractor provideLoginInteractor() {
        return new LoginInteractorImpl();
    }

    @Provides
    public LoginDataSource provideLoginDataSource() {
        return new LoginDataSourceImpl();
    }

    @Provides
    public HomeDataSource provideHomeDataSource() {
        return new HomeDataSourceImpl();
    }
}
