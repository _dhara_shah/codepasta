package com.dhara.android.codepasta.home.datasource;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.ServiceError;
import com.dhara.android.codepasta.network.api.RestApi;
import com.dhara.android.codepasta.network.entity.Pasta;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomeDataSourceImpl implements HomeDataSource {
    @Inject
    RestApi restApiService;

    public HomeDataSourceImpl() {
        CodePastaApp.getInstance().getNetComponent().inject(this);
    }

    @Override
    public void fetchPastas(final String userName, final String userToken, final Listener<List<Pasta>> listener) {
        restApiService.getOwnRepositories(userName, userToken)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(listener::onSuccess)
                .doOnError(error -> listener.onError(new ServiceError(error.getMessage())))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void fetchCommunityPastas(final String userName, final String userToken, final Listener<List<Pasta>> listener) {
        restApiService.getRepositories(userName, userToken)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(listener::onSuccess)
                .doOnError(error -> listener.onError(new ServiceError(error.getMessage())))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }
}
