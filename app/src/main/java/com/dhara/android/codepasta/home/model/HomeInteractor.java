package com.dhara.android.codepasta.home.model;

import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.network.entity.Pasta;

import java.util.List;

public interface HomeInteractor {
    void loadPastas(String userName, String userToken, boolean owner, ResponseListener listener);

    List<Pasta> getPastas();
}
