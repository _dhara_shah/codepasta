package com.dhara.android.codepasta.home.router;

public interface HomeRouter {
    void close();

    void handleOnBackPress();

    void handleSignOut();
}
