package com.dhara.android.codepasta.dagger2;

import android.content.Context;

import com.dhara.android.codepasta.dagger2.components.AppComponent;

public interface AppComponentProvider {
    AppComponent getAppComponent(Context context);
}
