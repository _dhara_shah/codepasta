package com.dhara.android.codepasta.login.model;

import android.support.annotation.VisibleForTesting;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.login.datasource.LoginDataSource;
import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.ServiceError;
import com.dhara.android.codepasta.network.api.Api;
import com.dhara.android.codepasta.network.entity.User;
import com.dhara.android.codepasta.network.entity.UserAuth;
import com.dhara.android.codepasta.network.entity.UserResponse;

import javax.inject.Inject;

import retrofit2.Response;

public class LoginInteractorImpl implements LoginInteractor {
    @Inject
    LoginDataSource dataSource;

    private User user;

    @VisibleForTesting
    UserAuth userAuth;

    public LoginInteractorImpl() {
        CodePastaApp.getInstance().getCodePastaComponent().inject(this);
    }

    @Override
    public void setUserAuth(final UserAuth userAuth) {
        this.userAuth = userAuth;
    }

    @Override
    public void register(final ResponseListener listener) {
        dataSource.register(userAuth, new Listener<Response<UserResponse>>() {
            @Override
            public void onSuccess(final Response<UserResponse> response) {
                if (response.body() != null && response.body().getResult().equals(Api.RESULT_OK)) {
                    user = response.body().getUser();
                    listener.onSuccess();
                }
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onError(error);
            }
        });
    }

    @Override
    public User getUser() {
        return user;
    }
}
