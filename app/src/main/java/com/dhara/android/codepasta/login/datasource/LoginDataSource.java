package com.dhara.android.codepasta.login.datasource;

import android.support.annotation.NonNull;

import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.entity.UserAuth;
import com.dhara.android.codepasta.network.entity.UserResponse;

import retrofit2.Response;

public interface LoginDataSource {
    void register(UserAuth userAuth, @NonNull Listener<Response<UserResponse>> listener);
}
