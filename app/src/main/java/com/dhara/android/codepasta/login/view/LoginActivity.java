package com.dhara.android.codepasta.login.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.dhara.android.codepasta.BaseActivity;
import com.dhara.android.codepasta.R;
import com.dhara.android.codepasta.login.presenter.LoginPresenter;
import com.dhara.android.codepasta.login.router.LoginRouter;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity {
    @Inject
    LoginPresenter presenter;

    @Inject
    LoginView loginView;

    @Inject
    LoginRouter loginRouter;

    public static void startActivity(final Context context) {
        final Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_login);
        presenter.handleOnCreate(loginView, loginRouter);
    }
}
