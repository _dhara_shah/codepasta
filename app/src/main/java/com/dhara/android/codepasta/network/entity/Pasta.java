package com.dhara.android.codepasta.network.entity;

import com.google.gson.annotations.SerializedName;

public class Pasta {
    private long id;
    private String name;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("owner")
    private PastaOwner owner;
    private String description;
    @SerializedName("fork")
    private boolean forked;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public PastaOwner getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public boolean isForked() {
        return forked;
    }
}
