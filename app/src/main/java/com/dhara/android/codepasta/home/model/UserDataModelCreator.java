package com.dhara.android.codepasta.home.model;

import android.support.annotation.NonNull;

import com.dhara.android.codepasta.network.entity.User;

public class UserDataModelCreator {
    @NonNull
    public static UserDataModel create(@NonNull final User user) {
        return new UserDataModel.Builder(user)
                .build();
    }
}
