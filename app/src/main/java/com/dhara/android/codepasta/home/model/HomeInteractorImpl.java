package com.dhara.android.codepasta.home.model;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.home.datasource.HomeDataSource;
import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.ServiceError;
import com.dhara.android.codepasta.network.entity.Pasta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class HomeInteractorImpl implements HomeInteractor {
    @Inject
    HomeDataSource dataSource;

    List<Pasta> pastaList;

    public HomeInteractorImpl() {
        CodePastaApp.getInstance().getCodePastaComponent().inject(this);
        pastaList = new ArrayList<>();
    }

    @Override
    public void loadPastas(final String userName,
                           final String userToken,
                           final boolean owner,
                           final ResponseListener listener) {

        if (owner) {
            fetchOwnPastas(userName, userToken, listener);
        } else {
            fetchCommunityPasta(userName, userToken, listener);
        }
    }

    @Override
    public List<Pasta> getPastas() {
        return pastaList == null ? Collections.emptyList() : pastaList;
    }

    private void fetchOwnPastas(String userName, String userToken, ResponseListener listener) {
        dataSource.fetchPastas(userName, userToken, new Listener<List<Pasta>>() {
            @Override
            public void onSuccess(final List<Pasta> response) {
                pastaList.clear();
                pastaList.addAll(response);
                listener.onSuccess();
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onError(error);
            }
        });
    }

    private void fetchCommunityPasta(String userName, String userToken, ResponseListener listener) {
        dataSource.fetchCommunityPastas(userName, userToken, new Listener<List<Pasta>>() {
            @Override
            public void onSuccess(final List<Pasta> response) {
                pastaList.clear();
                pastaList.addAll(response);
                listener.onSuccess();
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onError(error);
            }
        });
    }
}

