package com.dhara.android.codepasta.dagger2;

import android.content.Context;

import com.dhara.android.codepasta.dagger2.components.AppComponent;

public class AppInjector {
    private AppInjector() {

    }

    public static AppComponent from(Context context) {
        final AppComponentProvider provider = (AppComponentProvider) context.getApplicationContext();
        return provider.getAppComponent(context);
    }
}
