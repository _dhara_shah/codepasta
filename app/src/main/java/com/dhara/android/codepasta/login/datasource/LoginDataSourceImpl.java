package com.dhara.android.codepasta.login.datasource;

import android.support.annotation.NonNull;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.ServiceError;
import com.dhara.android.codepasta.network.api.RestApi;
import com.dhara.android.codepasta.network.entity.UserAuth;
import com.dhara.android.codepasta.network.entity.UserResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginDataSourceImpl implements LoginDataSource {
    @Inject
    RestApi restApi;

    public LoginDataSourceImpl() {
        CodePastaApp.getInstance().getNetComponent().inject(this);
    }

    @Override
    public void register(final UserAuth userAuth, @NonNull final Listener<Response<UserResponse>> listener) {
        restApi.register(userAuth).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(final Call<UserResponse> call, final Response<UserResponse> response) {
                listener.onSuccess(response);
            }

            @Override
            public void onFailure(final Call<UserResponse> call, final Throwable t) {
                listener.onError(new ServiceError(t.getMessage()));
            }
        });
    }
}
