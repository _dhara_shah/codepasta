package com.dhara.android.codepasta.login.presenter;

import com.dhara.android.codepasta.login.router.LoginRouter;
import com.dhara.android.codepasta.login.view.LoginView;

public interface LoginPresenter {
    void handleOnCreate(LoginView view, LoginRouter router);
}
