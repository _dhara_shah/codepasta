package com.dhara.android.codepasta.utils;

import android.util.Log;

import com.dhara.android.codepasta.BuildConfig;
import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.dagger2.AppInjector;

public class CodePastaLog {
    public CodePastaLog() {
        AppInjector.from(CodePastaApp.getInstance()).inject(this);
    }

    public void v(final String tag, final String msg) {
        if (useLog()) {
            Log.v(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public void d(final String tag, final String msg) {
        if (useLog()) {
            Log.d(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public void e(final String tag, final String msg) {
        if (useLog()) {
            Log.e(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public void wtf(final String tag, final Throwable throwable) {
        if (useLog()) {
            Log.wtf(String.valueOf(tag), throwable);
        }
    }

    private static boolean useLog() {
        return BuildConfig.DEBUG;
    }
}