package com.dhara.android.codepasta.home.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.dhara.android.codepasta.BaseActivity;
import com.dhara.android.codepasta.R;
import com.dhara.android.codepasta.home.model.UserDataModel;
import com.dhara.android.codepasta.home.model.UserDataModelCreator;
import com.dhara.android.codepasta.home.presenter.HomePresenter;
import com.dhara.android.codepasta.home.router.HomeRouter;
import com.dhara.android.codepasta.network.entity.User;
import com.dhara.android.codepasta.utils.ConstantIntentExtra;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity {

    @Inject
    HomePresenter presenter;

    @Inject
    HomeRouter router;

    @Inject
    HomeView homeView;

    public static void startActivity(Context context, Bundle bundle) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_home);

        UserDataModel model = null;
        if (getIntent().getExtras() != null) {
            final User user = getIntent().getParcelableExtra(ConstantIntentExtra.USER_EXTRA);
            model = UserDataModelCreator.create(user).setOwner(true);
        }

        presenter.handleOnCreate(model);
        presenter.attach(homeView, router);
    }

    @Override
    public void onBackPressed() {
        presenter.handleOnBackPress();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
