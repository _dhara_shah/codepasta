package com.dhara.android.codepasta.dagger2.modules;

import android.app.Application;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.utils.CodePastaLog;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class AppModule {
    private final CodePastaApp application;

    public AppModule(final CodePastaApp application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    public CodePastaLog provideCodePastaLog() {
        return new CodePastaLog();
    }
}
