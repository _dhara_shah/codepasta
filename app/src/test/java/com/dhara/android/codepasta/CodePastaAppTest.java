package com.dhara.android.codepasta;

import android.support.annotation.CallSuper;

import com.dhara.android.codepasta.dagger2.components.CodePastaComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerCodePastaComponent;
import com.dhara.android.codepasta.dagger2.modules.CodePastaModule;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CodePastaApp.class)
public class CodePastaAppTest extends CoreBaseTest {
    @Mock
    protected CodePastaModule codePastaModule;
    protected CodePastaComponent codePastaComponent;

    @Override
    @Before
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();
        codePastaComponent = DaggerCodePastaComponent
                .builder()
                .codePastaModule(codePastaModule)
                .build();
        PowerMockito.mockStatic(CodePastaApp.class);
        when(CodePastaApp.getInstance()).thenReturn(codePastaApp);
        PowerMockito.when(codePastaApp.getCodePastaComponent()).thenReturn(codePastaComponent);
    }
}
