package com.dhara.android.codepasta.network.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {
    @SerializedName("user_token")
    private String userToken;
    @SerializedName("username")
    private String userName;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("repositories_count")
    private long repositories;

    public User() {}

    protected User(Parcel in) {
        userToken = in.readString();
        userName = in.readString();
        name = in.readString();
        email = in.readString();
        repositories = in.readLong();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userToken);
        dest.writeString(userName);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeLong(repositories);
    }

    public String getUserName() {
        return userName;
    }

    public String getUserToken() {
        return userToken;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
