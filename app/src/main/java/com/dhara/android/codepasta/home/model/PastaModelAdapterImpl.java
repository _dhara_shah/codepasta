package com.dhara.android.codepasta.home.model;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.view.View;

import com.dhara.android.codepasta.R;
import com.dhara.android.codepasta.network.entity.Pasta;

import java.util.List;

public class PastaModelAdapterImpl implements PastaModelAdapter {
    private final List<Pasta> pastaList;
    private final UserDataModel model;

    public static PastaModelAdapterImpl createUsing(@NonNull final List<Pasta> pastaList,
                                                    final UserDataModel model) {
        return new PastaModelAdapterImpl(pastaList, model);
    }

    private PastaModelAdapterImpl(@NonNull final List<Pasta> pastaList, final UserDataModel model) {
        this.pastaList = pastaList;
        this.model = model;
    }

    @Override
    public int getCount() {
        if (pastaList.isEmpty()) {
            return 0;
        }
        return pastaList.size();
    }

    @NonNull
    @Override
    public String getPastaName(final int position) {
        return pastaList.get(position).getName();
    }

    @NonNull
    @Override
    public String getPastaDescription(final int position) {
        return pastaList.get(position).getDescription();
    }

    @NonNull
    @Override
    public String getPastaOwnerName(final int position) {
        return pastaList.get(position).getOwner().getOwnerName();
    }

    @ColorRes
    @Override
    public int getBackgroundColor(final int position) {
        if (hasFork(position)) {
            return R.color.colorDullGreen;
        }
        return R.color.colorPrimary;
    }

    @Override
    public String getAvatar(final int position) {
        return pastaList.get(position).getOwner().getAvatarUrl();
    }

    @Override
    public String getId(final int position) {
        return String.valueOf(pastaList.get(position).getId());
    }

    @Override
    public String getName() {
        return model.getUser().getName();
    }

    @Override
    public String getEmail() {
        return model.getUser().getEmail();
    }

    @Override
    public int getFabVisibility() {
        return model.isOwner() ? View.VISIBLE : View.GONE;
    }

    @Visibility
    @Override
    public int getForksVisibility(final int position) {
        return hasFork(position) ? View.VISIBLE : View.GONE;
    }

    private boolean hasFork(final int position) {
        return pastaList.get(position).isForked();
    }
}
