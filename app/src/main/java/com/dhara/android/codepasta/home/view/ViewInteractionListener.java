package com.dhara.android.codepasta.home.view;

public interface ViewInteractionListener {
    void onCommPastaSelected();

    void onMyPastasSelected();

    void onSignedOut();
}
