package com.dhara.android.codepasta.login.presenter;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.login.model.LoginInteractor;
import com.dhara.android.codepasta.login.router.LoginRouter;
import com.dhara.android.codepasta.login.view.LoginView;
import com.dhara.android.codepasta.login.view.ViewInteractionListener;
import com.dhara.android.codepasta.network.ServiceError;
import com.dhara.android.codepasta.network.entity.UserAuth;

import javax.inject.Inject;

public class LoginPresenterImpl implements LoginPresenter, ViewInteractionListener, ResponseListener {
    @Inject
    LoginInteractor loginInteractor;

    private LoginView view;
    private LoginRouter router;

    public LoginPresenterImpl() {
        CodePastaApp.getInstance().getCodePastaComponent().inject(this);
    }

    @Override
    public void handleOnCreate(LoginView view, LoginRouter router) {
        this.view = view;
        this.router = router;
        this.view.initViews(this);
    }

    @Override
    public void onLoginClicked(final UserAuth userAuth) {
        loginInteractor.setUserAuth(userAuth);
        loginInteractor.register(this);
    }

    @Override
    public void onRegisterClicked() {
        router.navigateToRegistration();
    }

    @Override
    public void onSuccess() {
        router.navigateToHome(loginInteractor.getUser());
    }

    @Override
    public void onError(ServiceError error) {
        view.showErrorMessage(error.getErrorMessage());
    }
}
