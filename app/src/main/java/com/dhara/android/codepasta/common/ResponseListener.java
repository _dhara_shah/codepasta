package com.dhara.android.codepasta.common;

import com.dhara.android.codepasta.network.ServiceError;

public interface ResponseListener {
    void onSuccess();

    void onError(ServiceError error);
}