package com.dhara.android.codepasta.home.model;

import android.support.annotation.NonNull;

import com.dhara.android.codepasta.network.entity.User;

public class UserDataModel {
    private User user;
    private boolean owner;

    public UserDataModel(final User user) {
        this.user = user;
    }

    public UserDataModel setOwner(final boolean owner) {
        this.owner = owner;
        return this;
    }

    public boolean isOwner() {
        return owner;
    }

    public User getUser() {
        return user;
    }

    static class Builder {
        private User user;

        public Builder(@NonNull final User user) {
            this.user = user;
        }

        public UserDataModel build() {
            return new UserDataModel(user);
        }
    }
}
