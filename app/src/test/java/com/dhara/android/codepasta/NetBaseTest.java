package com.dhara.android.codepasta;

import android.app.Application;
import android.support.annotation.CallSuper;

import com.dhara.android.codepasta.dagger2.AppInjector;
import com.dhara.android.codepasta.dagger2.components.AppComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerAppComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerNetComponent;
import com.dhara.android.codepasta.dagger2.components.NetComponent;
import com.dhara.android.codepasta.dagger2.modules.AppModule;
import com.dhara.android.codepasta.dagger2.modules.NetModule;
import com.dhara.android.codepasta.network.api.RestApi;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Retrofit.class, Cache.class})
public class NetBaseTest extends CodePastaAppTest {
    @Mock
    protected NetModule netModule;
    @Mock
    protected AppModule appModule;
    @Mock
    protected Retrofit retrofit;
    @Mock
    protected OkHttpClient client;
    @Mock
    protected Application application;
    @Mock
    protected Cache cache;
    @Mock
    protected RestApi apiService;
    protected NetComponent netComponent;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);
        netComponent = DaggerNetComponent
                .builder()
                .appModule(appModule)
                .netModule(netModule)
                .build();

        PowerMockito.mockStatic(AppInjector.class, Retrofit.class, Cache.class);
        when(netModule.provideRetrofit(Matchers.any(Gson.class), Matchers.any(OkHttpClient.class))).thenReturn(retrofit);
        when(netModule.provideGson()).thenReturn(new Gson());
        when(netModule.provideHttpCache(application)).thenReturn(cache);
        when(netModule.provideOkhttpClient(cache)).thenReturn(client);
        when(CodePastaApp.getInstance()).thenReturn(codePastaApp);
        when(codePastaApp.getNetComponent()).thenReturn(netComponent);
        when(netModule.getApiService(retrofit)).thenReturn(apiService);
    }
}
