package com.dhara.android.codepasta.login.model;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.NetBaseTest;
import com.dhara.android.codepasta.common.ResponseListener;
import com.dhara.android.codepasta.login.datasource.LoginDataSource;
import com.dhara.android.codepasta.network.Listener;
import com.dhara.android.codepasta.network.ServiceError;
import com.dhara.android.codepasta.network.entity.UserAuth;
import com.dhara.android.codepasta.network.entity.UserResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import retrofit2.Response;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Response.class, CodePastaApp.class})
public class LoginInteractorImplTest extends NetBaseTest {
    /*private LoginInteractorImpl interactor;

    @Mock
    LoginDataSource loginDataSource;
    @Mock
    ResponseListener responseListener;
    @Mock
    ServiceError serviceError;
    @Captor
    ArgumentCaptor<Listener<Response<UserResponse>>> userResponseListener;
    @Mock
    UserAuth userAuth;
    @Mock
    Response<UserResponse> response;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Response.class, CodePastaApp.class);
        when(CodePastaApp.getInstance()).thenReturn(codePastaApp);
        when(codePastaApp.getCodePastaComponent()).thenReturn(codePastaComponent);
        when(codePastaApp.getNetComponent()).thenReturn(netComponent);
        when(codePastaModule.provideLoginDataSource()).thenReturn(loginDataSource);
        interactor = new LoginInteractorImpl();
        interactor.userAuth = userAuth;
    }

    @Ignore
    @Test
    public void testRegisterSuccess() {
        interactor.register(responseListener);
        verify(loginDataSource).register(eq(userAuth), userResponseListener.capture());
        userResponseListener.getValue().onSuccess(response);
        verify(responseListener).onSuccess();
    }

    @Ignore
    @Test
    public void testRegisterError() {
        interactor.register(responseListener);
        verify(loginDataSource).register(eq(userAuth), userResponseListener.capture());
        userResponseListener.getValue().onError(serviceError);
        verify(responseListener).onError(serviceError);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(loginDataSource, responseListener, response);
    } */
}
