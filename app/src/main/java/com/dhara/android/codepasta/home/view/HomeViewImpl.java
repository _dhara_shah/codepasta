package com.dhara.android.codepasta.home.view;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dhara.android.codepasta.R;
import com.dhara.android.codepasta.home.model.PastaModelAdapter;

import javax.inject.Inject;

public class HomeViewImpl implements HomeView, NavigationView.OnNavigationItemSelectedListener {
    private AppCompatActivity activity;
    private DrawerLayout drawer;
    private View progressView;
    private RecyclerView rvRepositories;
    private ViewInteractionListener listener;

    @Inject
    public HomeViewImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(@NonNull final PastaModelAdapter modelAdapter, final ViewInteractionListener listener) {
        this.listener = listener;
        setUpToolbar();

        rvRepositories = activity.findViewById(R.id.rv_repositories);
        progressView = activity.findViewById(R.id.lnr_progress_view);

        final FloatingActionButton fab = activity.findViewById(R.id.fab);

        fab.setVisibility(modelAdapter.getFabVisibility());
        fab.setOnClickListener(view -> Snackbar.make(view, "Should lead to the Create Pasta screen!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        final NavigationView navigationView = activity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final View headerView = navigationView.getHeaderView(0);
        final TextView txtName = headerView.findViewById(R.id.txt_name);
        txtName.setText(modelAdapter.getName());

        final TextView txtEmail = headerView.findViewById(R.id.txt_email);
        txtEmail.setText(modelAdapter.getEmail());

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRepositories.setLayoutManager(linearLayoutManager);
        rvRepositories.setHasFixedSize(false);

        final PastaAdapter adapter = PastaAdapter.createUsing(modelAdapter);
        rvRepositories.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(rvRepositories, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void updateData(@NonNull final PastaModelAdapter modelAdapter) {
        final PastaAdapter adapter = PastaAdapter.createUsing(modelAdapter);
        rvRepositories.swapAdapter(adapter, false);
    }

    @Override
    public boolean closeDrawerLayout() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_my_pastas) {
            listener.onMyPastasSelected();
        } else if (id == R.id.nav_community_pastas) {
            listener.onCommPastaSelected();
        } else if (id == R.id.nav_signout) {
            listener.onSignedOut();
        } else if (id == R.id.nav_share) {

        }

        drawer = activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setUpToolbar() {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        drawer = activity.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }
}
