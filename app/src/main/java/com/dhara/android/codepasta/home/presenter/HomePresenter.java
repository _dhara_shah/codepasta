package com.dhara.android.codepasta.home.presenter;

import com.dhara.android.codepasta.home.model.UserDataModel;
import com.dhara.android.codepasta.home.router.HomeRouter;
import com.dhara.android.codepasta.home.view.HomeView;

public interface HomePresenter {
    void handleOnCreate(UserDataModel model);

    void attach(HomeView view, HomeRouter router);

    void handleOnBackPress();
}
