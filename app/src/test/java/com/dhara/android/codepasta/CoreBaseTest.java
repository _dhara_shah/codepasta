package com.dhara.android.codepasta;

import android.app.Application;
import android.content.Context;
import android.support.annotation.CallSuper;

import com.dhara.android.codepasta.dagger2.AppInjector;
import com.dhara.android.codepasta.dagger2.components.AppComponent;
import com.dhara.android.codepasta.dagger2.components.DaggerAppComponent;
import com.dhara.android.codepasta.dagger2.modules.AppModule;
import com.dhara.android.codepasta.dagger2.modules.NetModule;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AppInjector.class, CodePastaApp.class})
public class CoreBaseTest {
    @Mock
    protected AppModule appModule;
    @Mock
    public CodePastaApp codePastaApp;

    protected AppComponent appComponent;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        appComponent = DaggerAppComponent
                .builder()
                .appModule(appModule)
                .build();
        PowerMockito.mockStatic(AppInjector.class,  CodePastaApp.class);
        PowerMockito.when(AppInjector.from(any(Context.class))).thenReturn(appComponent);
        when(CodePastaApp.getInstance()).thenReturn(codePastaApp);
    }

    @Test
    public void emptyTest() {
        //to compile with Runner
    }
}