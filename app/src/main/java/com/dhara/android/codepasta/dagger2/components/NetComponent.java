package com.dhara.android.codepasta.dagger2.components;

import com.dhara.android.codepasta.dagger2.modules.AppModule;
import com.dhara.android.codepasta.dagger2.modules.NetModule;
import com.dhara.android.codepasta.home.datasource.HomeDataSourceImpl;
import com.dhara.android.codepasta.login.datasource.LoginDataSourceImpl;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(LoginDataSourceImpl loginDataSource);

    void inject(HomeDataSourceImpl homeDataSource);
}
