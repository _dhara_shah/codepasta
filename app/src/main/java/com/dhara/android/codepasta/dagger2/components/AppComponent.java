package com.dhara.android.codepasta.dagger2.components;

import android.app.Application;

import com.dhara.android.codepasta.CodePastaApp;
import com.dhara.android.codepasta.dagger2.modules.AppModule;
import com.dhara.android.codepasta.utils.CodePastaLog;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(CodePastaApp codePastaApp);

    void inject(CodePastaLog codePastaLog);

    Application application();

    CodePastaLog getCodePastaLog();
}
